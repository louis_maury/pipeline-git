<?php

require_once __DIR__.'/../vendor/autoload.php';
include_once(__DIR__."/../src/City.php");

class CityTest extends \PHPUnit\Framework\TestCase
{
    public function testgetCityNameById()
    {
        $city = new City();
        $result = $city->getCityNameById(1);
        $expected = 'Bordeaux';
        $this->assertTrue($result == $expected);

        $test2 = $city->getCityNameById(2);
        $expected2 = 'Paname';
        $this->assertTrue($test2 == $expected2);
    }
}
